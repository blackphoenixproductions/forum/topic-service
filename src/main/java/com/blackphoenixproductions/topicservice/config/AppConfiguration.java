package com.blackphoenixproductions.topicservice.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfiguration {
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    public NewTopic topic() {
        return TopicBuilder.name("refresh-mv-topics")
                .partitions(10)
                .replicas(1)
                .build();
    }

}
