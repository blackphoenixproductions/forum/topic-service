package com.blackphoenixproductions.topicservice.domain.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Topic {
    private Long id;
    private User author;
    private String title;
    private String message;
    private boolean pinned;
    private boolean emailUser;
    private LocalDateTime createDate;
    private LocalDateTime deleteDate;
    private LocalDateTime editDate;
    private Long postsNumber;
}
