package com.blackphoenixproductions.topicservice.domain.service;

import com.blackphoenixproductions.topicservice.domain.ITopicDAO;
import com.blackphoenixproductions.topicservice.domain.ITopicService;
import com.blackphoenixproductions.topicservice.domain.model.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


@Service
public class TopicService implements ITopicService {

    private final ITopicDAO topicDAO;

    @Autowired
    public TopicService(ITopicDAO topicDAO) {
        this.topicDAO = topicDAO;
    }

    @Override
    public Topic createTopic(Topic topic) {
        topic.setCreateDate(LocalDateTime.now());
        return topicDAO.createTopic(topic);
    }

    @Override
    public Topic editTopic(Topic topic) {
        topic.setEditDate(LocalDateTime.now());
        return topicDAO.editTopic(topic);
    }

}
