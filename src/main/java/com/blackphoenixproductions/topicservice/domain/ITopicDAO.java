package com.blackphoenixproductions.topicservice.domain;

import com.blackphoenixproductions.commons.dto.Filter;
import com.blackphoenixproductions.topicservice.domain.model.Topic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface ITopicDAO {
    Long count();
    Topic createTopic(Topic topic);
    Topic editTopic(Topic topic);
    Page<Topic> getPagedTopics(Pageable pageable, Filter filter);
    Long getTotalTopics();
    Topic getTopic(Long id);
    void refreshMaterializedView();
}
