package com.blackphoenixproductions.topicservice.domain;

import com.blackphoenixproductions.topicservice.domain.model.Topic;


public interface ITopicService {
    Topic createTopic(Topic topic);
    Topic editTopic(Topic topic);
}
