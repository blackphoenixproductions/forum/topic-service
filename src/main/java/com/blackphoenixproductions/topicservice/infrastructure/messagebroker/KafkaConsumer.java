package com.blackphoenixproductions.topicservice.infrastructure.messagebroker;

import com.blackphoenixproductions.commons.constants.Groups;
import com.blackphoenixproductions.commons.constants.Topics;
import com.blackphoenixproductions.topicservice.domain.ITopicDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);
    private final ITopicDAO topicDAO;

    @Autowired
    public KafkaConsumer(ITopicDAO topicDAO) {
        this.topicDAO = topicDAO;
    }


    @KafkaListener(groupId = Groups.TOPICS_CONSUMER, topics = Topics.REFRESH_MV_TOPICS)
    public void listen(String message) {
        logger.info("Receiving message : {}", message);
        topicDAO.refreshMaterializedView();
    }

}
