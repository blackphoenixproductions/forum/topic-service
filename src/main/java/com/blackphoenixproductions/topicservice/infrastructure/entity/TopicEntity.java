package com.blackphoenixproductions.topicservice.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.Objects;


@Entity
@Table(name = "TOPICS")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TopicEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;

    @Column
    private String message;

    @Column
    private boolean pinned;

    @Column
    private boolean emailUser;

    @Column
    private LocalDateTime createDate;

    @Column
    private LocalDateTime deleteDate;

    @Column
    private LocalDateTime editDate;

    @Column
    private Long user_id;


    public TopicEntity(Long id) {
        this.id = id;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TopicEntity topic = (TopicEntity) o;
        return id.equals(topic.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
