package com.blackphoenixproductions.topicservice.infrastructure.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Immutable;


import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Immutable
@Entity
@Table(name = "MV_TOPICS")
public class MVTopicEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;

    @Column
    private String message;

    @Column
    private boolean pinned;

    @Column
    private boolean emailUser;

    @Column
    private LocalDateTime createDate;

    @Column
    private LocalDateTime deleteDate;

    @Column
    private LocalDateTime editDate;

    @Column
    private Long authorId;

    @Column
    private String authorUsername;

    @Column
    private String authorEmail;

    @Column
    private Long postsNumber;

}
