package com.blackphoenixproductions.topicservice.infrastructure.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema(description = "Il topic da creare.")
public class InsertTopicDTO {
    @NotEmpty(message = "Il titolo del topic non puo' essere null/vuoto")
    @Size(min = 5, max = 50, message = "Il titolo del topic puo' avere un minimo di 5 caratteri e un massimo di 50")
    @Schema(description = "Il titolo del topic")
    private String title;
    @NotEmpty(message = "Il messaggio del topic non puo' essere null/vuoto")
    @Size(max = 20000, message = "Il messaggio del topic ha superato il massimo di 20000 caratteri")
    @Schema(description = "Il messaggio del topic.")
    private String message;
    @Schema(description = "Indica se dovrà essere messo in evidenza.")
    private boolean pinned;
    @Schema(description = "Indica se l'utente vuole ricevere delle mail ad ogni risposta.")
    private boolean emailUser;
    @Schema(description = "L'id dell'utente creatore del topic")
    @NotNull(message = "L'id dell'utente non puo' essere null.")
    private Long userId;
}
