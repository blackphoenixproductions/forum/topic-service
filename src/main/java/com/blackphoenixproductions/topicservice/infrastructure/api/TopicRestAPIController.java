package com.blackphoenixproductions.topicservice.infrastructure.api;


import com.blackphoenixproductions.commons.dto.Filter;
import com.blackphoenixproductions.topicservice.domain.ITopicDAO;
import com.blackphoenixproductions.topicservice.domain.ITopicService;
import com.blackphoenixproductions.topicservice.domain.model.Topic;
import com.blackphoenixproductions.topicservice.infrastructure.api.dto.EditTopicDTO;
import com.blackphoenixproductions.topicservice.infrastructure.api.dto.InsertTopicDTO;
import com.blackphoenixproductions.topicservice.infrastructure.mappers.TopicMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;


@RestController
@RequestMapping("/topics")
@Tag(name = "Topic", description = "endpoints riguardanti i topic.")
public class TopicRestAPIController {

    private static final Logger logger = LoggerFactory.getLogger(TopicRestAPIController.class);
    private final ITopicService topicService;
    private final ITopicDAO topicDAO;
    private final TopicMapper topicMapper;


    @Autowired
    public TopicRestAPIController(ITopicService topicService, TopicMapper topicMapper, ITopicDAO topicDAO) {
        this.topicService = topicService;
        this.topicDAO = topicDAO;
        this.topicMapper = topicMapper;
    }


    @Operation(summary = "Restituisce il numero totale dei topic.")
    @GetMapping(value = "/public/getTotalTopics")
    public ResponseEntity<Long> getTotalTopics (HttpServletRequest req, Principal principal){
        Long totalTopics = topicDAO.getTotalTopics();
        return new ResponseEntity<>(totalTopics, HttpStatus.OK);
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(hidden = true))),
    })
    @Operation(summary = "Ricerca di un topic con paginazione e filtro dinamico.")
    @PostMapping(value = "/public/findFilteredTopicsByPage")
    public ResponseEntity<Page<Topic>> findFilteredTopicsByPage (@ParameterObject @PageableDefault(sort = {"createDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                                                                                    @RequestBody (required = false) Filter filter){
        Page<Topic> pagedTopics = topicDAO.getPagedTopics(pageable, filter);
        return new ResponseEntity<>(pagedTopics, HttpStatus.OK);
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema = @Schema(hidden = true))),
    })
    @Operation(summary = "Ricerca di un topic.")
    @GetMapping(value = "/public/findTopic")
    public ResponseEntity<Topic> findTopic (@Parameter(description = "L'id del topic da cercare.") @RequestParam Long id){
        Topic topic = topicDAO.getTopic(id);
        return new ResponseEntity<>(topic, HttpStatus.OK);
    }


    @PreAuthorize("hasAnyAuthority('USER', 'STAFF', 'HELPDESK')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema = @Schema(hidden = true))),
    })
    @Operation(summary = "Creazione di un topic.")
    @PostMapping(value = "createTopic")
    public ResponseEntity<Topic> createTopic(@RequestBody @Valid InsertTopicDTO insertTopicDTO){
        logger.info("Start createTopic");
        Topic savedTopic = topicService.createTopic(topicMapper.insertTopicDTOtoTopic(insertTopicDTO));
        logger.info("End createTopic");
        return new ResponseEntity<>(savedTopic, HttpStatus.OK);
    }


    @PreAuthorize("hasAnyAuthority('USER', 'STAFF', 'HELPDESK')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema = @Schema(hidden = true))),
    })
    @Operation(summary = "Modifica di un topic.")
    @PostMapping(value = "editTopic")
    public ResponseEntity<Topic> editTopic(@RequestBody @Valid EditTopicDTO topicDTO){
        logger.info("Start editTopic");
        Topic editedTopic = topicService.editTopic(topicMapper.editTopicDTOtoTopic(topicDTO));
        logger.info("End editTopic");
        return new ResponseEntity<>(editedTopic, HttpStatus.OK);
    }

}
