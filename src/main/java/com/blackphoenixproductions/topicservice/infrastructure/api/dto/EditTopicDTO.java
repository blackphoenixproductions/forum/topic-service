package com.blackphoenixproductions.topicservice.infrastructure.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Schema(description = "Il topic da modificare.")
public class EditTopicDTO {
    @NotNull(message = "L'id del topic non puo' essere null")
    @Schema(description = "L'id del topic.")
    private Long id;
    @NotEmpty(message = "Il messaggio del topic non puo' essere null/vuoto")
    @Size(max = 20000, message = "Il messaggio del topic ha superato il massimo di 20000 caratteri")
    @Schema(description = "Il messaggio del topic.")
    private String message;
}
