package com.blackphoenixproductions.topicservice.infrastructure;

import com.blackphoenixproductions.commons.dto.Filter;
import org.springframework.data.jpa.domain.Specification;

public interface ISpecificationBuilder {
    <T> Specification<T> getSpecification(Filter filter);
}
