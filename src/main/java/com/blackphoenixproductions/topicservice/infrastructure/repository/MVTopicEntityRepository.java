package com.blackphoenixproductions.topicservice.infrastructure.repository;


import com.blackphoenixproductions.topicservice.infrastructure.entity.MVTopicEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


public interface MVTopicEntityRepository extends JpaRepository<MVTopicEntity, Long>, JpaSpecificationExecutor<MVTopicEntity> {
    @Modifying
    @Query(value = "REFRESH MATERIALIZED VIEW CONCURRENTLY mv_topics WITH DATA", nativeQuery=true)
    void refreshMVTopics();
}
