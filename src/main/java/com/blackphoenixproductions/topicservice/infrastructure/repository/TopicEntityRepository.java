package com.blackphoenixproductions.topicservice.infrastructure.repository;


import com.blackphoenixproductions.topicservice.infrastructure.entity.TopicEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TopicEntityRepository extends JpaRepository<TopicEntity, Long> {
}