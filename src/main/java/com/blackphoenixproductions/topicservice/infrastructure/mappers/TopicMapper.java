package com.blackphoenixproductions.topicservice.infrastructure.mappers;


import com.blackphoenixproductions.topicservice.domain.model.Topic;
import com.blackphoenixproductions.topicservice.infrastructure.api.dto.EditTopicDTO;
import com.blackphoenixproductions.topicservice.infrastructure.api.dto.InsertTopicDTO;
import com.blackphoenixproductions.topicservice.infrastructure.entity.MVTopicEntity;
import com.blackphoenixproductions.topicservice.infrastructure.entity.TopicEntity;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface TopicMapper {
    @Mapping(target = "author.id", source = "userId")
    Topic insertTopicDTOtoTopic(InsertTopicDTO insertTopicDTO);
    Topic editTopicDTOtoTopic(EditTopicDTO editTopicDTO);
    @Mapping(target = "author.id", source = "authorId")
    @Mapping(target = "author.username", source = "authorUsername")
    @Mapping(target = "author.email", source = "authorEmail")
    Topic mvTopicEntitytoTopic(MVTopicEntity mvTopicEntity);
    @Mapping(target = "author.id", source = "user_id")
    Topic topicEntitytoTopic(TopicEntity topicEntity);
    @Mapping(target = "user_id", source = "author.id")
    TopicEntity topicToTopicEntity(Topic topic);
}
