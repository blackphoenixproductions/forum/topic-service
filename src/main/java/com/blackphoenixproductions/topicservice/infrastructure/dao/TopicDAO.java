package com.blackphoenixproductions.topicservice.infrastructure.dao;

import com.blackphoenixproductions.commons.dto.Filter;
import com.blackphoenixproductions.commons.exception.CustomException;
import com.blackphoenixproductions.topicservice.domain.ITopicDAO;
import com.blackphoenixproductions.topicservice.domain.model.Topic;
import com.blackphoenixproductions.topicservice.infrastructure.ISpecificationBuilder;
import com.blackphoenixproductions.topicservice.infrastructure.entity.MVTopicEntity;
import com.blackphoenixproductions.topicservice.infrastructure.entity.TopicEntity;
import com.blackphoenixproductions.topicservice.infrastructure.mappers.TopicMapper;
import com.blackphoenixproductions.topicservice.infrastructure.repository.MVTopicEntityRepository;
import com.blackphoenixproductions.topicservice.infrastructure.repository.TopicEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Repository
public class TopicDAO implements ITopicDAO {

    private static final Logger logger = LoggerFactory.getLogger(TopicDAO.class);
    private final MVTopicEntityRepository mvTopicEntityRepository;
    private final TopicEntityRepository topicEntityRepository;
    private final ISpecificationBuilder specificationBuilder;
    private final TopicMapper topicMapper;

    @Autowired
    public TopicDAO(MVTopicEntityRepository mvTopicEntityRepository, TopicEntityRepository topicEntityRepository, ISpecificationBuilder specificationBuilder, TopicMapper topicMapper) {
        this.mvTopicEntityRepository = mvTopicEntityRepository;
        this.topicEntityRepository = topicEntityRepository;
        this.specificationBuilder = specificationBuilder;
        this.topicMapper = topicMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public Long count() {
        return topicEntityRepository.count();
    }

    @Transactional
    @Override
    public Topic createTopic(Topic topic) {
        TopicEntity entity = topicMapper.topicToTopicEntity(topic);
        TopicEntity savedTopic = topicEntityRepository.saveAndFlush(entity);
        return topicMapper.topicEntitytoTopic(savedTopic);
    }

    @Transactional
    @Override
    public Topic editTopic(Topic topic) {
        Optional<TopicEntity> findedTopic = topicEntityRepository.findById(topic.getId());
        if (!findedTopic.isPresent()){
            throw new CustomException("Topic not found", HttpStatus.NOT_FOUND);
        }
        TopicEntity topicToEdit = findedTopic.get();
        topicToEdit.setMessage(topic.getMessage());
        topicToEdit.setEditDate(topic.getEditDate());
        TopicEntity savedTopic = topicEntityRepository.saveAndFlush(topicToEdit);
        return topicMapper.topicEntitytoTopic(savedTopic);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Topic> getPagedTopics(Pageable pageable, Filter filter) {
        Specification<MVTopicEntity> spec = null;
        if (filter != null && filter.getFilters() != null && !filter.getFilters().isEmpty()) {
            spec = specificationBuilder.getSpecification(filter);
        }
        Page<MVTopicEntity> pageMvTopicEntity = mvTopicEntityRepository.findAll(spec, pageable);
        Page<Topic> topicPage = pageMvTopicEntity.map(mvTopicEntity -> topicMapper.mvTopicEntitytoTopic(mvTopicEntity));
        return topicPage;
    }

    @Transactional(readOnly = true)
    @Override
    public Long getTotalTopics() {
        return topicEntityRepository.count();
    }

    @Transactional(readOnly = true)
    @Override
    public Topic getTopic(Long id) {
        return topicMapper.topicEntitytoTopic(topicEntityRepository.findById(id).get());
    }

    @Transactional
    @Override
    public void refreshMaterializedView() {
        logger.info("Refreshing MV_TOPICS...");
        mvTopicEntityRepository.refreshMVTopics();
        logger.info("MV_TOPICS refreshed");
    }
}
