CREATE DATABASE topic;

create table TOPICS (
    id bigserial primary key not null,
    title varchar(255),
    pinned boolean not null DEFAULT false,
    email_user boolean not null DEFAULT false,
    create_date timestamp,
    delete_date timestamp,
    edit_date timestamp,
    message text,
    user_id bigint not null
);


CREATE EXTENSION postgres_fdw;

CREATE SERVER postgres_fdw_post FOREIGN DATA WRAPPER postgres_fdw
OPTIONS (host 'localhost', dbname 'post');
CREATE USER MAPPING FOR PUBLIC SERVER postgres_fdw_post
OPTIONS (password '');
CREATE FOREIGN TABLE posts (id bigserial, message text, create_date timestamp, delete_date timestamp, edit_date timestamp, user_id bigint not null, topic_id bigint not null)
SERVER postgres_fdw_post
OPTIONS (table_name 'posts');

CREATE SERVER postgres_fdw_user FOREIGN DATA WRAPPER postgres_fdw
OPTIONS (host 'localhost', dbname 'user');
CREATE USER MAPPING FOR PUBLIC SERVER postgres_fdw_user
OPTIONS (password '');
CREATE FOREIGN TABLE users (id bigserial, username varchar(255), email varchar(255), role varchar(255))
SERVER postgres_fdw_user
OPTIONS (table_name 'users');


CREATE MATERIALIZED VIEW MV_TOPICS AS
select t.id, t.title, t.message, t.pinned, t.email_user,
       t.create_date, t.delete_date, t.edit_date, u.username as author_username, u.email as author_email, u.id as author_id,
       (select count(*) from posts p, topics t2 where t2.id = t.id and p.topic_id = t2.id) as posts_number
from TOPICS t, USERS u
where t.user_id = u.id;

CREATE UNIQUE INDEX ON MV_TOPICS(id);

CREATE OR REPLACE FUNCTION refresh_mv_topics()
   RETURNS trigger
   LANGUAGE plpgsql
AS
$$
BEGIN
	REFRESH MATERIALIZED VIEW CONCURRENTLY mv_topics with data;
    RETURN NULL;
END;
$$;

CREATE TRIGGER trigger_refresh_mv_topics AFTER UPDATE OR INSERT OR DELETE ON topics
FOR EACH STATEMENT
EXECUTE PROCEDURE refresh_mv_topics();